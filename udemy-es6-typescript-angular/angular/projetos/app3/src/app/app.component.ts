import { Component, OnInit } from "@angular/core";
import * as firebase from "firebase";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";

  ngOnInit(): void {
    this.initFirebase();
  }

  public initFirebase() {
    const firebaseConfig = {
      apiKey: "AIzaSyAGUsGU_H6_Sm1OOeEb0Ti6w0f5z6FD440",
      authDomain: "jta-instagram-clone-7d770.firebaseapp.com",
      databaseURL: "https://jta-instagram-clone-7d770.firebaseio.com",
      projectId: "jta-instagram-clone-7d770",
      storageBucket: "jta-instagram-clone-7d770.appspot.com",
      messagingSenderId: "838918413348",
      appId: "1:838918413348:web:a0914c9b4ae8667ba6ccb1"
    };

    firebase.initializeApp(firebaseConfig);
  }
}
