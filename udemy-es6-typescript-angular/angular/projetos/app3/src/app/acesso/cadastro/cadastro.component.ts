import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Usuario } from "../usuario.model";

import { AutenticacaoService } from "../../autenticacao.service";

@Component({
  selector: "app-cadastro",
  templateUrl: "./cadastro.component.html",
  styleUrls: ["./cadastro.component.css"]
})
export class CadastroComponent implements OnInit {
  @Output() public exibirLogin: EventEmitter<string> = new EventEmitter<
    string
  >();
  public usuario: Usuario;

  public formulario = new FormGroup({
    email: new FormControl(""),
    nome: new FormControl(""),
    usuario: new FormControl(""),
    senha: new FormControl("")
  });

  constructor(public autenticacao: AutenticacaoService) {}

  ngOnInit() {}

  public exibirLoginPainel(): void {
    this.exibirLogin.emit("login");
  }

  public cadastrarForm(): void {
    let usuario = new Usuario(
      this.formulario.value.email,
      this.formulario.value.nome,
      this.formulario.value.usuario,
      this.formulario.value.senha
    );

    this.autenticacao.cadastrarUsuario(usuario);
  }
}
